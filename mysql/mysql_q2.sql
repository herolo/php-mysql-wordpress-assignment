TABLE fsia
   companyName VARCHAR (30) NOT NULL PRIMARY KEY
   marketCapitalization FLOAT NOT NULL

TABLE fsib
   companyName VARCHAR (30) NOT NULL PRIMARY KEY
   sharePrice FLOAT NOT NULL
   shareOutstanding  INTEGER NOT NULL
